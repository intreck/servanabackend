-- MySQL dump 10.13  Distrib 8.0.13, for Win64 (x86_64)
--
-- Host: localhost    Database: servana_new
-- ------------------------------------------------------
-- Server version	8.0.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `oauth_client_details`
--

DROP TABLE IF EXISTS `oauth_client_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `oauth_client_details` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `client_id` varchar(250) NOT NULL,
  `resource_ids` varchar(250) DEFAULT NULL,
  `client_secret` varchar(250) DEFAULT NULL,
  `scope` varchar(250) DEFAULT NULL,
  `authorized_grant_types` varchar(250) DEFAULT NULL,
  `web_server_redirect_uri` varchar(250) DEFAULT NULL,
  `authorities` varchar(250) DEFAULT NULL,
  `access_token_validity` int(11) DEFAULT NULL,
  `refresh_token_validity` int(11) DEFAULT NULL,
  `additional_information` varchar(4096) DEFAULT NULL,
  `autoapprove` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `client_id_UNIQUE` (`client_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_client_details`
--

LOCK TABLES `oauth_client_details` WRITE;
/*!40000 ALTER TABLE `oauth_client_details` DISABLE KEYS */;
INSERT INTO `oauth_client_details` VALUES (1,'crmClient1','oauth2-resource','{bcrypt}$2a$10$5ilijwW/hk74fu7EABpaJ.NdG4uq1Xehv9.htmeXCiiAU.2fkdQ1S','read,write','password,refresh_token',NULL,'ROLE_CLIENT,ROLE_TRUSTED_CLIENT',9000,2592000,NULL,NULL);
/*!40000 ALTER TABLE `oauth_client_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sarvena_rfq_service_provider`
--

DROP TABLE IF EXISTS `sarvena_rfq_service_provider`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `sarvena_rfq_service_provider` (
  `Sarvena_RFQ_Service_Provider_Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `RFQ_ID` int(11) NOT NULL,
  `SP_ID` int(11) NOT NULL,
  `Status` bit(1) NOT NULL,
  `Bid_Limit` varchar(100) NOT NULL,
  PRIMARY KEY (`Sarvena_RFQ_Service_Provider_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sarvena_rfq_service_provider`
--

LOCK TABLES `sarvena_rfq_service_provider` WRITE;
/*!40000 ALTER TABLE `sarvena_rfq_service_provider` DISABLE KEYS */;
/*!40000 ALTER TABLE `sarvena_rfq_service_provider` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `servana_header_names`
--

DROP TABLE IF EXISTS `servana_header_names`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `servana_header_names` (
  `Servana_Header_Names_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `Haeder_Id` varchar(150) DEFAULT NULL,
  `Header_Name` varchar(150) DEFAULT NULL,
  `Description` varchar(250) DEFAULT NULL,
  `Status` bit(1) NOT NULL,
  PRIMARY KEY (`Servana_Header_Names_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `servana_header_names`
--

LOCK TABLES `servana_header_names` WRITE;
/*!40000 ALTER TABLE `servana_header_names` DISABLE KEYS */;
/*!40000 ALTER TABLE `servana_header_names` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `servana_hospital`
--

DROP TABLE IF EXISTS `servana_hospital`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `servana_hospital` (
  `Servana_Hospital_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `Hospital_ID` bigint(20) NOT NULL,
  `HS_Status` bit(1) DEFAULT NULL,
  `HS_Name` varchar(250) DEFAULT NULL,
  `HS_Description` varchar(500) DEFAULT NULL,
  `HS_Phone` varchar(10) DEFAULT NULL,
  `HS_Website` varchar(50) DEFAULT NULL,
  `HS_Address1` varchar(250) DEFAULT NULL,
  `HS_Address2` varchar(250) DEFAULT NULL,
  `HS_City` varchar(150) DEFAULT NULL,
  `HS_Country` varchar(150) DEFAULT NULL,
  `HS_ZipCode` varchar(150) DEFAULT NULL,
  `HS_Employees_No` bigint(20) NOT NULL,
  `HS_Established_Year` int(11) DEFAULT NULL,
  `Created_By` bigint(20) DEFAULT NULL,
  `Created_At` datetime DEFAULT NULL,
  `Updated_By` bigint(20) DEFAULT NULL,
  `Updated_At` datetime DEFAULT NULL,
  `Deleted_By` bigint(20) DEFAULT NULL,
  `Deleted_At` datetime DEFAULT NULL,
  PRIMARY KEY (`Servana_Hospital_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `servana_hospital`
--

LOCK TABLES `servana_hospital` WRITE;
/*!40000 ALTER TABLE `servana_hospital` DISABLE KEYS */;
/*!40000 ALTER TABLE `servana_hospital` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `servana_hp_gallary`
--

DROP TABLE IF EXISTS `servana_hp_gallary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `servana_hp_gallary` (
  `Servana_HP_Gallary_ID` bigint(20) NOT NULL,
  `Gallary_ID` varchar(150) DEFAULT NULL,
  `Hospital_ID` bigint(20) NOT NULL,
  `Status` bit(1) NOT NULL,
  `Picture_Name` varchar(150) DEFAULT NULL,
  `Picture_Path` varchar(250) DEFAULT NULL,
  `Video_Name` varchar(150) DEFAULT NULL,
  `Video_Path` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`Servana_HP_Gallary_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `servana_hp_gallary`
--

LOCK TABLES `servana_hp_gallary` WRITE;
/*!40000 ALTER TABLE `servana_hp_gallary` DISABLE KEYS */;
/*!40000 ALTER TABLE `servana_hp_gallary` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `servana_hp_login_users`
--

DROP TABLE IF EXISTS `servana_hp_login_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `servana_hp_login_users` (
  `Login_Users_Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `Email_ID` varchar(150) DEFAULT NULL,
  `Password` varchar(250) DEFAULT NULL,
  `Is_Verified` bit(1) NOT NULL,
  `Role_ID` bigint(20) DEFAULT NULL,
  `Created_By` bigint(20) DEFAULT NULL,
  `Created_At` datetime DEFAULT NULL,
  `Updated_By` bigint(20) DEFAULT NULL,
  `Updated_At` datetime DEFAULT NULL,
  `Deleted_By` bigint(20) DEFAULT NULL,
  `Deleted_At` datetime DEFAULT NULL,
  `Is_Active` bit(1) DEFAULT NULL,
  `Is_Account_Non_Expired` bit(1) DEFAULT NULL,
  `Is_Account_Non_Locked` bit(1) DEFAULT NULL,
  `Is_Credentials_Non_Expired` bit(1) DEFAULT NULL,
  `token` varchar(100) DEFAULT NULL,
  `reset_password_token` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`Login_Users_Id`),
  UNIQUE KEY `Email_ID` (`Email_ID`),
  KEY `Servana_HP_Login_Users_ROLE_ID_FK` (`Role_ID`),
  CONSTRAINT `Servana_HP_Login_Users_ROLE_ID_FK` FOREIGN KEY (`Role_ID`) REFERENCES `servana_user_role` (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `servana_hp_login_users`
--

LOCK TABLES `servana_hp_login_users` WRITE;
/*!40000 ALTER TABLE `servana_hp_login_users` DISABLE KEYS */;
INSERT INTO `servana_hp_login_users` VALUES (1,'servana2019@gmail.com','{bcrypt}$2a$10$I5M0ykFNAh7lqNSX5W2F1uZSDCKFblviqM2o8X7dMpDkma.TH/VNu',_binary '',6,1,'2019-03-15 09:09:09',NULL,NULL,NULL,NULL,_binary '',_binary '',_binary '',_binary '',NULL,NULL);
/*!40000 ALTER TABLE `servana_hp_login_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `servana_hp_user_details`
--

DROP TABLE IF EXISTS `servana_hp_user_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `servana_hp_user_details` (
  `User_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `Email_ID` varchar(150) DEFAULT NULL,
  `Status` bit(1) NOT NULL,
  `Hospital_ID` bigint(20) NOT NULL,
  `First_Name` varchar(150) NOT NULL,
  `Last_Name` varchar(150) NOT NULL,
  `Profile_Picture` tinyblob,
  `Primary_Phone` varchar(10) NOT NULL,
  `Secondary_Phone` varchar(10) DEFAULT NULL,
  `Address_1` varchar(150) NOT NULL,
  `Address_2` varchar(150) DEFAULT NULL,
  `City` varchar(100) NOT NULL,
  `State` varchar(100) NOT NULL,
  `Country` varchar(100) NOT NULL,
  `Zip_Code` varchar(10) NOT NULL,
  `Created_By` bigint(20) DEFAULT NULL,
  `Created_At` datetime DEFAULT NULL,
  `Updated_By` bigint(20) DEFAULT NULL,
  `Updated_At` datetime DEFAULT NULL,
  `Deleted_By` bigint(20) DEFAULT NULL,
  `Deleted_At` datetime DEFAULT NULL,
  PRIMARY KEY (`User_ID`),
  UNIQUE KEY `Email_ID` (`Email_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `servana_hp_user_details`
--

LOCK TABLES `servana_hp_user_details` WRITE;
/*!40000 ALTER TABLE `servana_hp_user_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `servana_hp_user_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `servana_hs_rfq`
--

DROP TABLE IF EXISTS `servana_hs_rfq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `servana_hs_rfq` (
  `Servana_HS_RFQ_Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `RFQ_ID` bigint(20) NOT NULL,
  `Hospital_ID` bigint(20) NOT NULL,
  `User_Id` bigint(20) NOT NULL,
  `RFQ_Status` varchar(250) NOT NULL,
  `RFQ_Title` varchar(250) NOT NULL,
  `RFQ_Category_ID` bigint(20) NOT NULL,
  `Service_Onsite` bit(1) NOT NULL,
  `Service_Offsite` bit(1) NOT NULL,
  `Service_Mode_Id` bigint(20) NOT NULL,
  `Questions_Status` varchar(250) NOT NULL,
  `Template_ID` bigint(20) NOT NULL,
  `RFQ_Scope` varchar(250) NOT NULL,
  `RFQ_Start_Date` datetime NOT NULL,
  `Final_Submission_Date` datetime NOT NULL,
  `Award_Date` datetime NOT NULL,
  `Contract_Date` datetime NOT NULL,
  `Wrok_Commensment_Date` datetime NOT NULL,
  `Renewal_Reminder_date` datetime NOT NULL,
  `RFQ_Submission_Mode` varchar(250) NOT NULL,
  `RFQ_Final_Document` tinyblob,
  `RFQ_Document_Path` varchar(250) NOT NULL,
  `Update_Id` bigint(20) NOT NULL,
  `Update_Date` datetime NOT NULL,
  PRIMARY KEY (`Servana_HS_RFQ_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `servana_hs_rfq`
--

LOCK TABLES `servana_hs_rfq` WRITE;
/*!40000 ALTER TABLE `servana_hs_rfq` DISABLE KEYS */;
/*!40000 ALTER TABLE `servana_hs_rfq` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `servana_login_history`
--

DROP TABLE IF EXISTS `servana_login_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `servana_login_history` (
  `Servana_Login_History_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `Email_ID` varchar(150) DEFAULT NULL,
  `Login_Timestamp` datetime NOT NULL,
  `Login_Status` bit(1) NOT NULL,
  PRIMARY KEY (`Servana_Login_History_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `servana_login_history`
--

LOCK TABLES `servana_login_history` WRITE;
/*!40000 ALTER TABLE `servana_login_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `servana_login_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `servana_rfq_attributes`
--

DROP TABLE IF EXISTS `servana_rfq_attributes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `servana_rfq_attributes` (
  `Servana_RFQ_Attributes_Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `RFQ_ID` int(11) NOT NULL,
  `Attribute_Name` varchar(250) NOT NULL,
  `Description` varchar(500) DEFAULT NULL,
  `Status` bit(1) NOT NULL,
  PRIMARY KEY (`Servana_RFQ_Attributes_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `servana_rfq_attributes`
--

LOCK TABLES `servana_rfq_attributes` WRITE;
/*!40000 ALTER TABLE `servana_rfq_attributes` DISABLE KEYS */;
/*!40000 ALTER TABLE `servana_rfq_attributes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `servana_rfq_master_category`
--

DROP TABLE IF EXISTS `servana_rfq_master_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `servana_rfq_master_category` (
  `Servana_RFQ_Master_Category_Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `Mas_Category_Id` bigint(20) NOT NULL,
  `Mas_Category_Desc` varchar(500) NOT NULL,
  `Status` bit(1) NOT NULL,
  PRIMARY KEY (`Servana_RFQ_Master_Category_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `servana_rfq_master_category`
--

LOCK TABLES `servana_rfq_master_category` WRITE;
/*!40000 ALTER TABLE `servana_rfq_master_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `servana_rfq_master_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `servana_rfq_questions`
--

DROP TABLE IF EXISTS `servana_rfq_questions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `servana_rfq_questions` (
  `Servana_RFQ_Questions_Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `RFQ_ID` int(11) NOT NULL,
  `Question` varchar(250) NOT NULL,
  `Description` varchar(500) DEFAULT NULL,
  `Status` bit(1) NOT NULL,
  PRIMARY KEY (`Servana_RFQ_Questions_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `servana_rfq_questions`
--

LOCK TABLES `servana_rfq_questions` WRITE;
/*!40000 ALTER TABLE `servana_rfq_questions` DISABLE KEYS */;
/*!40000 ALTER TABLE `servana_rfq_questions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `servana_rfq_reminders`
--

DROP TABLE IF EXISTS `servana_rfq_reminders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `servana_rfq_reminders` (
  `Servana_RFQ_Reminders_Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `RFQ_ID` int(11) NOT NULL,
  `Reminder_Start_Date` datetime NOT NULL,
  `Reminder_Submission_Date` datetime NOT NULL,
  `Reminder_Award_Date` datetime NOT NULL,
  `Reminder_Contract_Date` datetime NOT NULL,
  `Reminder_Work_Comm_Date` datetime NOT NULL,
  `Reminder_Renewal_Date` datetime NOT NULL,
  PRIMARY KEY (`Servana_RFQ_Reminders_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `servana_rfq_reminders`
--

LOCK TABLES `servana_rfq_reminders` WRITE;
/*!40000 ALTER TABLE `servana_rfq_reminders` DISABLE KEYS */;
/*!40000 ALTER TABLE `servana_rfq_reminders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `servana_rfq_service_location`
--

DROP TABLE IF EXISTS `servana_rfq_service_location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `servana_rfq_service_location` (
  `Servana_RFQ_Service_Location_Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `RFQ_ID` int(11) NOT NULL,
  `Status` bit(1) NOT NULL,
  `Address1` varchar(250) NOT NULL,
  `Address2` varchar(250) DEFAULT NULL,
  `City` varchar(150) NOT NULL,
  `State` varchar(150) NOT NULL,
  `Country` varchar(150) NOT NULL,
  `ZipCode` varchar(10) NOT NULL,
  PRIMARY KEY (`Servana_RFQ_Service_Location_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `servana_rfq_service_location`
--

LOCK TABLES `servana_rfq_service_location` WRITE;
/*!40000 ALTER TABLE `servana_rfq_service_location` DISABLE KEYS */;
/*!40000 ALTER TABLE `servana_rfq_service_location` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `servana_rfq_service_mode`
--

DROP TABLE IF EXISTS `servana_rfq_service_mode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `servana_rfq_service_mode` (
  `Servana_RFQ_Service_Mode_Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `Service_Mode_Id` varchar(150) DEFAULT NULL,
  `Service_Mode_Name` varchar(150) DEFAULT NULL,
  `Description` varchar(250) DEFAULT NULL,
  `Status` bit(1) NOT NULL,
  PRIMARY KEY (`Servana_RFQ_Service_Mode_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `servana_rfq_service_mode`
--

LOCK TABLES `servana_rfq_service_mode` WRITE;
/*!40000 ALTER TABLE `servana_rfq_service_mode` DISABLE KEYS */;
/*!40000 ALTER TABLE `servana_rfq_service_mode` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `servana_rfq_stages`
--

DROP TABLE IF EXISTS `servana_rfq_stages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `servana_rfq_stages` (
  `Servana_RFQ_Stages_Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `RFQ_ID` bigint(20) NOT NULL,
  `Overall_Status` varchar(250) NOT NULL,
  `Category_Status` varchar(250) NOT NULL,
  `Titlle_Status` varchar(250) NOT NULL,
  `Location_Status` varchar(250) NOT NULL,
  `Template_Status` varchar(250) NOT NULL,
  `Scope_Status` varchar(250) NOT NULL,
  `Attributes_Status` varchar(250) NOT NULL,
  `Questions_Status` varchar(250) NOT NULL,
  `Upload_Status` varchar(250) NOT NULL,
  `Submission_Status` varchar(250) NOT NULL,
  `SP_Selection_Status` varchar(250) NOT NULL,
  `Review_Status` varchar(250) NOT NULL,
  `Publish_Status` varchar(250) NOT NULL,
  `Update_Id` bigint(20) NOT NULL,
  `Update_Date` datetime NOT NULL,
  PRIMARY KEY (`Servana_RFQ_Stages_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `servana_rfq_stages`
--

LOCK TABLES `servana_rfq_stages` WRITE;
/*!40000 ALTER TABLE `servana_rfq_stages` DISABLE KEYS */;
/*!40000 ALTER TABLE `servana_rfq_stages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `servana_rfq_templates`
--

DROP TABLE IF EXISTS `servana_rfq_templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `servana_rfq_templates` (
  `Template_Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `Template_Name` varchar(250) NOT NULL,
  `Description` varchar(500) NOT NULL,
  `Template_Type` varchar(250) NOT NULL,
  `Status` bit(1) NOT NULL,
  `Create_ID` bigint(20) NOT NULL,
  `Update_Id` bigint(20) NOT NULL,
  `Create_Date` datetime NOT NULL,
  `Update_Date` datetime NOT NULL,
  PRIMARY KEY (`Template_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `servana_rfq_templates`
--

LOCK TABLES `servana_rfq_templates` WRITE;
/*!40000 ALTER TABLE `servana_rfq_templates` DISABLE KEYS */;
/*!40000 ALTER TABLE `servana_rfq_templates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `servana_rfq_uploads`
--

DROP TABLE IF EXISTS `servana_rfq_uploads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `servana_rfq_uploads` (
  `Servana_RFQ_Uploads_Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `RFQ_ID` int(11) NOT NULL,
  `File name` varchar(250) NOT NULL,
  `File_Heading` varchar(250) DEFAULT NULL,
  `Description` varchar(500) DEFAULT NULL,
  `File_Path` varchar(250) DEFAULT NULL,
  `Status` bit(1) NOT NULL,
  PRIMARY KEY (`Servana_RFQ_Uploads_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `servana_rfq_uploads`
--

LOCK TABLES `servana_rfq_uploads` WRITE;
/*!40000 ALTER TABLE `servana_rfq_uploads` DISABLE KEYS */;
/*!40000 ALTER TABLE `servana_rfq_uploads` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `servana_user_role`
--

DROP TABLE IF EXISTS `servana_user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `servana_user_role` (
  `Role_Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `Role_Name` varchar(50) DEFAULT NULL,
  `Description` varchar(150) DEFAULT NULL,
  `Status` bit(1) DEFAULT NULL,
  `Created_At` datetime DEFAULT NULL,
  `Created_By` bigint(20) DEFAULT NULL,
  `Updated_At` datetime DEFAULT NULL,
  `Updated_By` bigint(20) DEFAULT NULL,
  `Deleted_At` datetime DEFAULT NULL,
  `Deleted_By` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`Role_Id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `servana_user_role`
--

LOCK TABLES `servana_user_role` WRITE;
/*!40000 ALTER TABLE `servana_user_role` DISABLE KEYS */;
INSERT INTO `servana_user_role` VALUES (6,'ROLE_ADMIN','ROLE_ADMIN',_binary '','2019-03-15 11:10:15',1,NULL,NULL,NULL,NULL),(7,'ROLE_USER','ROLE_USER',_binary '','2019-03-15 11:10:15',1,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `servana_user_role` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-03-24 21:01:44
