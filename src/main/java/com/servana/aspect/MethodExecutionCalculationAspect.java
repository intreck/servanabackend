package com.servana.aspect;

import java.util.concurrent.TimeUnit;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@Aspect
@Configuration
@EnableAspectJAutoProxy(proxyTargetClass = true)
public class MethodExecutionCalculationAspect {

	private static final Logger logger = LoggerFactory.getLogger(MethodExecutionCalculationAspect.class);

	/**
	 * 
	 * @param joinPoint
	 * @return
	 * @throws Throwable
	 */
	@Around("@annotation(LogExecutionTime)")
	public Object around(ProceedingJoinPoint joinPoint) throws Throwable {
		logger.info(":: Entered into {} ::", joinPoint);
		long startTime = System.currentTimeMillis();

		Object proceed = joinPoint.proceed();

		long timeTaken = System.currentTimeMillis() - startTime;
		long mins = TimeUnit.MILLISECONDS.toSeconds(timeTaken);
		logger.info(":: Exited from {} and Time Taken is {} ::", joinPoint, mins);
		return proceed;
	}

}
