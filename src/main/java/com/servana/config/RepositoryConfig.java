package com.servana.config;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.servana.domain.ServanaHpLoginUsers;
import com.servana.service.IServanaHpLoginUsersService;

@Configuration
@EnableAutoConfiguration
@EntityScan(basePackages = { "com.servana.domain" })
@EnableJpaRepositories(basePackages = { "com.servana.repository" })
@EnableTransactionManagement
@EnableJpaAuditing(auditorAwareRef = "auditorProvider")
public class RepositoryConfig {

	@Autowired
	private IServanaHpLoginUsersService servanaHpLoginUsersService;

	@Bean
	public AuditorAware<Long> auditorProvider() {

		return new AuditorAware<Long>() {

			@Override
			public Optional<Long> getCurrentAuditor() {
				Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
				if (authentication == null || !authentication.isAuthenticated()) {
					return null;
				}
				System.out.println("========> name" + authentication.getName());
				ServanaHpLoginUsers user = servanaHpLoginUsersService.findByEmailId(authentication.getName());
				System.out.println("========> name" + user);
				if (user != null && user.getLoginUsersId() != null) {
					return Optional.of(user.getLoginUsersId());
				} else {
					return Optional.of(new Long(0));
				}

			}
		};

	}

}
