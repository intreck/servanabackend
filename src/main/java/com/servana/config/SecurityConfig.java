package com.servana.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.approval.ApprovalStore;
import org.springframework.security.oauth2.provider.approval.TokenApprovalStore;
import org.springframework.security.oauth2.provider.approval.TokenStoreUserApprovalHandler;
import org.springframework.security.oauth2.provider.request.DefaultOAuth2RequestFactory;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

import static com.servana.constant.ApplicationConstant.JWT_SIGNING_KEY;

@Configuration
@EnableWebSecurity
@ComponentScan({ "com.servana.*" })
public class SecurityConfig  extends WebSecurityConfigurerAdapter {

	@Autowired
	private ClientDetailsService clientDetailsService;
	
	@Autowired
	private AuthenticationProvider authenticationProvider;

	@Override
    protected void configure(HttpSecurity http) throws Exception {
		http
			.sessionManagement()
			.sessionCreationPolicy(SessionCreationPolicy.STATELESS)
				.and()
			//.cors()
			//	.and()
			.csrf().disable()
		  	.authorizeRequests()
		  	.antMatchers("/pages/**").permitAll() //Like About Us, Services, Contact Us etc.
		  	.antMatchers("/sign-up", "/api1/**", "/newuser/**", "/api1/forgotPassword").permitAll()
		  	.antMatchers("/v2/api-docs", 
		  				 "/api-info/**", 
		  				 "/swagger-ui.html",
		  				 "/actuator/**",
		  				 "/webjars/**", 
		  				 "/swagger-resources/**").permitAll()
		  	.antMatchers(HttpMethod.OPTIONS, "/oauth/token").permitAll()
		  	.anyRequest().authenticated();
    }

    
    @Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
    	
    	auth.authenticationProvider(authenticationProvider);
	}
    
	
    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {    	
        return super.authenticationManagerBean();
    }	
	@Bean
	public TokenStore tokenStore() {		
		return new JwtTokenStore(jwtTokenEnhancer());
	}
	
	@Bean
	protected JwtAccessTokenConverter jwtTokenEnhancer() {
		JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
	    converter.setSigningKey(JWT_SIGNING_KEY);
	    return converter;
	}	

	@Bean
	@Autowired
	public TokenStoreUserApprovalHandler userApprovalHandler(TokenStore tokenStore){
		
		TokenStoreUserApprovalHandler handler = new TokenStoreUserApprovalHandler();
		handler.setTokenStore(tokenStore);
		handler.setRequestFactory(new DefaultOAuth2RequestFactory(clientDetailsService));
		handler.setClientDetailsService(clientDetailsService);
		return handler;
	}
	
	@Bean
	@Autowired
	public ApprovalStore approvalStore(TokenStore tokenStore) throws Exception {
		TokenApprovalStore store = new TokenApprovalStore();
		store.setTokenStore(tokenStore);
		return store;
	}
	
	@Bean
	public PasswordEncoder passwordEncoder() {
		return PasswordEncoderFactories.createDelegatingPasswordEncoder();		
	}	
	
}
