package com.servana.config;

import static com.servana.constant.ApplicationConstant.BASE_PACKAGE;
import static com.servana.constant.ApplicationConstant.DEFAULT_PRODUCES_AND_CONSUMES;
import static com.servana.constant.ApplicationConstant.CLIENT_ID;
import static com.servana.constant.ApplicationConstant.CLIENT_SECRET;
import static com.servana.constant.ApplicationConstant.AUTH_SERVER;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.AuthorizationCodeGrantBuilder;
import springfox.documentation.builders.OAuthBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.Contact;
import springfox.documentation.service.GrantType;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.service.SecurityScheme;
import springfox.documentation.service.TokenEndpoint;
import springfox.documentation.service.TokenRequestEndpoint;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.SecurityConfiguration;
import springfox.documentation.swagger.web.SecurityConfigurationBuilder;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2)
				.produces(new HashSet<String>(Arrays.asList(DEFAULT_PRODUCES_AND_CONSUMES)))
				.consumes(new HashSet<String>(Arrays.asList(DEFAULT_PRODUCES_AND_CONSUMES)))
				.select()
				.apis(RequestHandlerSelectors.basePackage(BASE_PACKAGE))
				.build().apiInfo(apiInfo())
				.securitySchemes(Arrays.asList(securityScheme()))
		        .securityContexts(Arrays.asList(securityContext()));
	}

	private final ApiInfo apiInfo() {
		return new ApiInfo(
				"Servana API", 
				"Servana API Description", 
				"1.0", "urn:tos", 
				contactInfo(), 
				"Apache 2.0",
				"http://www.apache.org/licenses/LICENSE-2.0", 
				Collections.emptyList());
	}
	
	private final Contact contactInfo() {
		return new Contact("Servana", "http://www.servana.com", "servana@gmail.com");
	}
	
	@Bean
    public SecurityConfiguration security() {
        return SecurityConfigurationBuilder.builder().clientId(CLIENT_ID).clientSecret(CLIENT_SECRET).useBasicAuthenticationWithAccessCodeGrant(true).build();
    }

    private SecurityScheme securityScheme() {
        GrantType grantType = new AuthorizationCodeGrantBuilder().tokenEndpoint(new TokenEndpoint(AUTH_SERVER + "/token", "oauthtoken")).tokenRequestEndpoint(new TokenRequestEndpoint(AUTH_SERVER + "/authorize", CLIENT_ID, CLIENT_ID)).build();

        SecurityScheme oauth = new OAuthBuilder().name("spring_oauth").grantTypes(Arrays.asList(grantType)).scopes(Arrays.asList(scopes())).build();
        return oauth;
    }

    private SecurityContext securityContext() {
        return SecurityContext.builder().securityReferences(Arrays.asList(new SecurityReference("spring_oauth", scopes()))).forPaths(PathSelectors.regex("/foos.*")).build();
    }

    private AuthorizationScope[] scopes() {
        AuthorizationScope[] scopes = { new AuthorizationScope("read", "for read operations"), new AuthorizationScope("write", "for write operations"), new AuthorizationScope("foo", "Access foo API") };
        return scopes;
    }
}