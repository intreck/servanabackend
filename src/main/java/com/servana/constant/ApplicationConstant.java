package com.servana.constant;

public class ApplicationConstant {

	public static final String SECRET = "SecretKeyToGenJWTs";
	public static final long EXPIRATION_TIME = 864_000_000; // 10 days
	public static final long ACCOUNT_EXPIRATION_TIME = 172_800_000; // 2 days
	public static final String TOKEN_PREFIX = "Bearer ";
	public static final String HEADER_STRING = "Authorization";
	public static final String BCRYPT_ID_FOR_ENCODE = "{bcrypt}";
	
	public static final String JWT_SIGNING_KEY = "SecretKeyToGenJWTs";
	
	public static final String BASE_PACKAGE = "com.servana.restapi";
	public static final String DEFAULT_PRODUCES_AND_CONSUMES = "application/json";
	
	public static final String AUTH_SERVER = "http://localhost:8080/servana/oauth";
    public static final String CLIENT_ID = "crmClient1";
    public static final String CLIENT_SECRET = "crmSuperSecret";
}
