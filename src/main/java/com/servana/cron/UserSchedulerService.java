package com.servana.cron;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.servana.aspect.LogExecutionTime;

@Component
public class UserSchedulerService {

	private final static Logger logger = LoggerFactory.getLogger(UserSchedulerService.class);

	/**
	 * NetCool
	 */
	@Scheduled(cron = "${deleteInactiveUserCronExpression}")
	@LogExecutionTime
	public void deleteInactiveUser() {
		logger.info("Current Thread : {}", Thread.currentThread().getName());

		try {

		} catch (Exception e) {
			logger.error(":: Exception occured in deleteInactiveUser() ::", e);
		} finally {

		}
	}
}