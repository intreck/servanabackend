package com.servana.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

/**
 * 
 * @author ANIL
 *
 */

@Getter
@Setter
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public abstract class Auditable<U> implements Serializable  {

	@Getter(AccessLevel.PRIVATE)
	private static final long serialVersionUID = 1L;

	@CreatedBy
	@Column(name = "Created_By", updatable = false)
	private U createdBy;
	
	@CreatedDate
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "Created_At", length = 19, updatable = false)
	private Date createdAt;
	
	@LastModifiedBy
	@Column(name = "Updated_By", insertable = false)
	private U updatedBy;	
	
	@LastModifiedDate
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "Updated_At", length = 19, insertable = false)
	private Date updatedAt;
	
	@Column(name = "Deleted_By", insertable = false)
	private U deletedBy;	
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "Deleted_At", length = 19, insertable = false)
	private Date deletedAt;
	
//	@Version
//	@Column(name = "version")
//    private Long version;
	
}
