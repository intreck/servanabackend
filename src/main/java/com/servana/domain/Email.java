package com.servana.domain;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@NoArgsConstructor
public class Email {
	private String subject;
	private String to;
	private String from;
	private String text;
}
