package com.servana.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * OauthClientDetails
 */
@Entity
@Table(name = "oauth_client_details", uniqueConstraints = @UniqueConstraint(columnNames = "client_id"))
public class OauthClientDetails implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7921996147580716915L;
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;
	@Column(name = "client_id", unique = true, nullable = false, length = 250)
	private String clientId;
	@Column(name = "resource_ids", length = 250)
	private String resourceIds;
	@Column(name = "client_secret", length = 250)
	private String clientSecret;
	@Column(name = "scope", length = 250)
	private String scope;
	@Column(name = "authorized_grant_types", length = 250)
	private String authorizedGrantTypes;
	@Column(name = "web_server_redirect_uri", length = 250)
	private String webServerRedirectUri;
	@Column(name = "authorities", length = 250)
	private String authorities;
	@Column(name = "access_token_validity")
	private Integer accessTokenValidity;
	@Column(name = "refresh_token_validity")
	private Integer refreshTokenValidity;
	@Column(name = "additional_information", length = 4096)
	private String additionalInformation;
	@Column(name = "autoapprove")
	private Boolean autoapprove;

}
