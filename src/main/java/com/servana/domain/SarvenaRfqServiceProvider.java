package com.servana.domain;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * SarvenaRfqServiceProvider
 */
@Entity
@Table(name = "sarvena_rfq_service_provider")
@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@ToString
public class SarvenaRfqServiceProvider implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2195118528475446886L;
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "Sarvena_RFQ_Service_Provider_Id", unique = true, nullable = false)
	private long sarvenaRfqServiceProviderId;
	@Column(name = "RFQ_ID", nullable = false)
	private int rfqId;
	@Column(name = "SP_ID", nullable = false)
	private int spId;
	@Column(name = "Status", nullable = false)
	private boolean status;
	@Column(name = "Bid_Limit", nullable = false, length = 100)
	private String bidLimit;

}
