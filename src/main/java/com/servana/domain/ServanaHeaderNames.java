package com.servana.domain;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * ServanaHeaderNames
 */
@Entity
@Table(name = "servana_header_names")
@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@ToString
public class ServanaHeaderNames implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4563736927810746658L;
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "Servana_Header_Names_ID", unique = true, nullable = false)
	private long servanaHeaderNamesId;
	@Column(name = "Haeder_Id", length = 150)
	private String haederId;
	@Column(name = "Header_Name", length = 150)
	private String headerName;
	@Column(name = "Description", length = 250)
	private String description;
	@Column(name = "Status", nullable = false)
	private boolean status;

}
