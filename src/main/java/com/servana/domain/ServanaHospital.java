package com.servana.domain;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * ServanaHospital
 */
@Entity
@Table(name = "servana_hospital")
@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@ToString
public class ServanaHospital extends Auditable<Long> implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8118557287689269548L;
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "Servana_Hospital_ID", unique = true, nullable = false)
	private Long servanaHospitalId;
	@Column(name = "Hospital_ID", nullable = false)
	private long hospitalId;
	@Column(name = "HS_Status")
	private Boolean hsStatus;
	@Column(name = "HS_Name", length = 250)
	private String hsName;
	@Column(name = "HS_Description", length = 500)
	private String hsDescription;
	@Column(name = "HS_Phone", length = 10)
	private String hsPhone;
	@Column(name = "HS_Website", length = 50)
	private String hsWebsite;
	@Column(name = "HS_Address1", length = 250)
	private String hsAddress1;
	@Column(name = "HS_Address2", length = 250)
	private String hsAddress2;
	@Column(name = "HS_City", length = 150)
	private String hsCity;
	@Column(name = "HS_Country", length = 150)
	private String hsCountry;
	@Column(name = "HS_ZipCode", length = 150)
	private String hsZipCode;
	@Column(name = "HS_Employees_No", nullable = false)
	private long hsEmployeesNo;
	@Column(name = "HS_Established_Year")
	private Integer hsEstablishedYear;

}
