package com.servana.domain;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * ServanaHpGallary
 */
@Entity
@Table(name = "servana_hp_gallary")
@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@ToString
public class ServanaHpGallary implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4576675695897167634L;
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "Servana_HP_Gallary_ID", unique = true, nullable = false)
	private long servanaHpGallaryId;
	@Column(name = "Gallary_ID", length = 150)
	private String gallaryId;
	@Column(name = "Hospital_ID", nullable = false)
	private long hospitalId;
	@Column(name = "Status", nullable = false)
	private boolean status;
	@Column(name = "Picture_Name", length = 150)
	private String pictureName;
	@Column(name = "Picture_Path", length = 250)
	private String picturePath;
	@Column(name = "Video_Name", length = 150)
	private String videoName;
	@Column(name = "Video_Path", length = 250)
	private String videoPath;

}
