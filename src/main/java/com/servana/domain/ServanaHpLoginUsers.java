package com.servana.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * ServanaHpLoginUsers
 */
@Entity
@Table(name = "servana_hp_login_users", uniqueConstraints = @UniqueConstraint(columnNames = "Email_ID"))
@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@ToString(exclude = { "servanaUserRole" })
@JsonIgnoreProperties(value = { "password", "isAccountNonExpired", "isAccountNonLocked", "isCredentialsNonExpired",
		"token", "resetPasswordToken", "createdBy", "createdAt", "updatedBy", "updatedAt", "deletedBy", "deletedAt" })
public class ServanaHpLoginUsers extends Auditable<Long> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5374097040118866660L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "Login_Users_Id", unique = true, nullable = false)
	@ApiModelProperty(notes = "The database generated User ID")
	private Long loginUsersId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "Role_ID")
	@ApiModelProperty(notes = "The User Role")
	private ServanaUserRole servanaUserRole;

	@Column(name = "Email_ID", unique = true, length = 150)
	@ApiModelProperty(notes = "The User Email ID")
	private String emailId;

	@Column(name = "Password", length = 250)
	@ApiModelProperty(notes = "User Password", required = true)
	private String password;

	@Column(name = "Is_Verified", nullable = false)
	private Boolean isVerified;

	@Column(name = "Is_Active")
	private Boolean isActive;

	@Column(name = "Is_Account_Non_Expired")
	private Boolean isAccountNonExpired;

	@Column(name = "Is_Account_Non_Locked")
	private Boolean isAccountNonLocked;

	@Column(name = "Is_Credentials_Non_Expired")
	private Boolean isCredentialsNonExpired;

	@Column(name = "Token", length = 100)
	private String token;

	@Column(name = "Reset_Password_Token", length = 250)
	@ApiModelProperty(notes = "User Reset Password  token")
	private String resetPasswordToken;

}
