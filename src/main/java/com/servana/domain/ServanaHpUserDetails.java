package com.servana.domain;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * ServanaHpUserDetails
 */
@Entity
@Table(name = "servana_hp_user_details", uniqueConstraints = @UniqueConstraint(columnNames = "Email_ID"))
@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@ToString
public class ServanaHpUserDetails extends Auditable<Long> implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9214924415673323780L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "User_ID", unique = true, nullable = false)
	private long userId;

	@Column(name = "Email_ID", unique = true, length = 150)
	private String emailId;
	
	@Column(name = "Status", nullable = false)
	private boolean status;

	@Column(name = "Hospital_ID", nullable = false)
	private long hospitalId;

	@Column(name = "First_Name", nullable = false, length = 150)
	private String firstName;

	@Column(name = "Last_Name", nullable = false, length = 150)
	private String lastName;

	@Lob
	@Column(name = "Profile_Picture", length = 100000)
	private byte[] profilePicture;

	@Column(name = "Primary_Phone", nullable = false, length = 10)
	private String primaryPhone;

	@Column(name = "Secondary_Phone", length = 10)
	private String secondaryPhone;

	@Column(name = "Address_1", nullable = false, length = 150)
	private String address1;

	@Column(name = "Address_2", length = 150)
	private String address2;

	@Column(name = "City", nullable = false, length = 100)
	private String city;

	@Column(name = "State", nullable = false, length = 100)
	private String state;

	@Column(name = "Country", nullable = false, length = 100)
	private String country;

	@Column(name = "Zip_Code", nullable = false, length = 10)
	private String zipCode;

}
