package com.servana.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * ServanaHsRfq
 */
@Entity
@Table(name = "servana_hs_rfq")
@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@ToString
public class ServanaHsRfq implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7408792399618953395L;
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "Servana_HS_RFQ_Id", unique = true, nullable = false)
	private long servanaHsRfqId;
	@Column(name = "RFQ_ID", nullable = false)
	private long rfqId;
	@Column(name = "Hospital_ID", nullable = false)
	private long hospitalId;
	@Column(name = "User_Id", nullable = false)
	private long userId;
	@Column(name = "RFQ_Status", nullable = false, length = 250)
	private String rfqStatus;
	@Column(name = "RFQ_Title", nullable = false, length = 250)
	private String rfqTitle;
	@Column(name = "RFQ_Category_ID", nullable = false)
	private long rfqCategoryId;
	@Column(name = "Service_Onsite", nullable = false)
	private boolean serviceOnsite;
	@Column(name = "Service_Offsite", nullable = false)
	private boolean serviceOffsite;
	@Column(name = "Service_Mode_Id", nullable = false)
	private long serviceModeId;
	@Column(name = "Questions_Status", nullable = false, length = 250)
	private String questionsStatus;
	@Column(name = "Template_ID", nullable = false)
	private long templateId;
	@Column(name = "RFQ_Scope", nullable = false, length = 250)
	private String rfqScope;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "RFQ_Start_Date", nullable = false, length = 19)
	private Date rfqStartDate;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "Final_Submission_Date", nullable = false, length = 19)
	private Date finalSubmissionDate;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "Award_Date", nullable = false, length = 19)
	private Date awardDate;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "Contract_Date", nullable = false, length = 19)
	private Date contractDate;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "Wrok_Commensment_Date", nullable = false, length = 19)
	private Date wrokCommensmentDate;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "Renewal_Reminder_date", nullable = false, length = 19)
	private Date renewalReminderDate;
	@Column(name = "RFQ_Submission_Mode", nullable = false, length = 250)
	private String rfqSubmissionMode;
	@Column(name = "RFQ_Final_Document")
	private byte[] rfqFinalDocument;
	@Column(name = "RFQ_Document_Path", nullable = false, length = 250)
	private String rfqDocumentPath;
	@Column(name = "Update_Id", nullable = false)
	private long updateId;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "Update_Date", nullable = false, length = 19)
	private Date updateDate;

}
