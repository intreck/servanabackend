package com.servana.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * ServanaLoginHistory
 */
@Entity
@Table(name = "servana_login_history")
@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@ToString
public class ServanaLoginHistory implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6263818524555861333L;
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "Servana_Login_History_ID", unique = true, nullable = false)
	private long servanaLoginHistoryId;
	@Column(name = "Email_ID", length = 150)
	private String userEmailId;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "Login_Timestamp", nullable = false, length = 19)
	private Date loginTimestamp;
	@Column(name = "Login_Status", nullable = false)
	private boolean loginStatus;
}
