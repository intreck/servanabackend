package com.servana.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * ServanaRfqAttributes
 */
@Entity
@Table(name = "servana_rfq_attributes")
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class ServanaRfqAttributes implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 749311418580507260L;
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "Servana_RFQ_Attributes_Id", unique = true, nullable = false)
	private Long servanaRfqAttributesId;
	@Column(name = "RFQ_ID", nullable = false)
	private int rfqId;
	@Column(name = "Attribute_Name", nullable = false, length = 250)
	private String attributeName;
	@Column(name = "Description", length = 500)
	private String description;
	@Column(name = "Status", nullable = false)
	private boolean status;

}
