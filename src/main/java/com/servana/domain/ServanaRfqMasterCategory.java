package com.servana.domain;


import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * ServanaRfqMasterCategory
 */
@Entity
@Table(name = "servana_rfq_master_category")
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class ServanaRfqMasterCategory implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2792736647013239471L;
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "Servana_RFQ_Master_Category_Id", unique = true, nullable = false)
	private long servanaRfqMasterCategoryId;
	@Column(name = "Mas_Category_Id", nullable = false)
	private long masCategoryId;
	@Column(name = "Mas_Category_Desc", nullable = false, length = 500)
	private String masCategoryDesc;
	@Column(name = "Status", nullable = false)
	private boolean status;
}
