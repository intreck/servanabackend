package com.servana.domain;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * ServanaRfqQuestions
 */
@Entity
@Table(name = "servana_rfq_questions")
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class ServanaRfqQuestions implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5261238929760293712L;
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "Servana_RFQ_Questions_Id", unique = true, nullable = false)
	private long servanaRfqQuestionsId;
	@Column(name = "RFQ_ID", nullable = false)
	private int rfqId;
	@Column(name = "Question", nullable = false, length = 250)
	private String question;
	@Column(name = "Description", length = 500)
	private String description;
	@Column(name = "Status", nullable = false)
	private boolean status;
}
