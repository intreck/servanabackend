package com.servana.domain;


import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * ServanaRfqReminders
 */
@Entity
@Table(name = "servana_rfq_reminders")
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class ServanaRfqReminders implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3880129595118700647L;
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "Servana_RFQ_Reminders_Id", unique = true, nullable = false)
	private long servanaRfqRemindersId;
	@Column(name = "RFQ_ID", nullable = false)
	private int rfqId;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "Reminder_Start_Date", nullable = false, length = 19)
	private Date reminderStartDate;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "Reminder_Submission_Date", nullable = false, length = 19)
	private Date reminderSubmissionDate;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "Reminder_Award_Date", nullable = false, length = 19)
	private Date reminderAwardDate;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "Reminder_Contract_Date", nullable = false, length = 19)
	private Date reminderContractDate;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "Reminder_Work_Comm_Date", nullable = false, length = 19)
	private Date reminderWorkCommDate;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "Reminder_Renewal_Date", nullable = false, length = 19)
	private Date reminderRenewalDate;

}
