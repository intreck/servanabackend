package com.servana.domain;


import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * ServanaRfqServiceLocation
 */
@Entity
@Table(name = "servana_rfq_service_location")
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class ServanaRfqServiceLocation implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4481129357711086255L;
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "Servana_RFQ_Service_Location_Id", unique = true, nullable = false)
	private long servanaRfqServiceLocationId;
	@Column(name = "RFQ_ID", nullable = false)
	private int rfqId;
	@Column(name = "Status", nullable = false)
	private boolean status;
	@Column(name = "Address1", nullable = false, length = 250)
	private String address1;
	@Column(name = "Address2", length = 250)
	private String address2;
	@Column(name = "City", nullable = false, length = 150)
	private String city;
	@Column(name = "State", nullable = false, length = 150)
	private String state;
	@Column(name = "Country", nullable = false, length = 150)
	private String country;
	@Column(name = "ZipCode", nullable = false, length = 10)
	private String zipCode;
}
