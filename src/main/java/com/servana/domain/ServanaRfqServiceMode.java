package com.servana.domain;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * ServanaRfqServiceMode
 */
@Entity
@Table(name = "servana_rfq_service_mode")
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class ServanaRfqServiceMode implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1205509569467937883L;
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "Servana_RFQ_Service_Mode_Id", unique = true, nullable = false)
	private long servanaRfqServiceModeId;
	@Column(name = "Service_Mode_Id", length = 150)
	private String serviceModeId;
	@Column(name = "Service_Mode_Name", length = 150)
	private String serviceModeName;
	@Column(name = "Description", length = 250)
	private String description;
	@Column(name = "Status", nullable = false)
	private boolean status;
}
