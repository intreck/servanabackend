package com.servana.domain;


import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * ServanaRfqStages
 */
@Entity
@Table(name = "servana_rfq_stages")
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class ServanaRfqStages implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3689861114681263365L;
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "Servana_RFQ_Stages_Id", unique = true, nullable = false)
	private long servanaRfqStagesId;
	@Column(name = "RFQ_ID", nullable = false)
	private long rfqId;
	@Column(name = "Overall_Status", nullable = false, length = 250)
	private String overallStatus;
	@Column(name = "Category_Status", nullable = false, length = 250)
	private String categoryStatus;
	@Column(name = "Titlle_Status", nullable = false, length = 250)
	private String titlleStatus;
	@Column(name = "Location_Status", nullable = false, length = 250)
	private String locationStatus;
	@Column(name = "Template_Status", nullable = false, length = 250)
	private String templateStatus;
	@Column(name = "Scope_Status", nullable = false, length = 250)
	private String scopeStatus;
	@Column(name = "Attributes_Status", nullable = false, length = 250)
	private String attributesStatus;
	@Column(name = "Questions_Status", nullable = false, length = 250)
	private String questionsStatus;
	@Column(name = "Upload_Status", nullable = false, length = 250)
	private String uploadStatus;
	@Column(name = "Submission_Status", nullable = false, length = 250)
	private String submissionStatus;
	@Column(name = "SP_Selection_Status", nullable = false, length = 250)
	private String spSelectionStatus;
	@Column(name = "Review_Status", nullable = false, length = 250)
	private String reviewStatus;
	@Column(name = "Publish_Status", nullable = false, length = 250)
	private String publishStatus;
	@Column(name = "Update_Id", nullable = false)
	private long updateId;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "Update_Date", nullable = false, length = 19)
	private Date updateDate;

}
