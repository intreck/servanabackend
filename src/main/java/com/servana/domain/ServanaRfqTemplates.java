package com.servana.domain;


import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * ServanaRfqTemplates
 */
@Entity
@Table(name = "servana_rfq_templates")
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class ServanaRfqTemplates implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7281721317506823339L;
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "Template_Id", unique = true, nullable = false)
	private long templateId;
	@Column(name = "Template_Name", nullable = false, length = 250)
	private String templateName;
	@Column(name = "Description", nullable = false, length = 500)
	private String description;
	@Column(name = "Template_Type", nullable = false, length = 250)
	private String templateType;
	@Column(name = "Status", nullable = false)
	private boolean status;
	@Column(name = "Create_ID", nullable = false)
	private long createId;
	@Column(name = "Update_Id", nullable = false)
	private long updateId;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "Create_Date", nullable = false, length = 19)
	private Date createDate;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "Update_Date", nullable = false, length = 19)
	private Date updateDate;
}
