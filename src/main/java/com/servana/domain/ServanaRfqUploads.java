package com.servana.domain;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * ServanaRfqUploads
 */
@Entity
@Table(name = "servana_rfq_uploads")
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class ServanaRfqUploads implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9141215301971638701L;
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "Servana_RFQ_Uploads_Id", unique = true, nullable = false)
	private long servanaRfqUploadsId;
	@Column(name = "RFQ_ID", nullable = false)
	private int rfqId;
	@Column(name = "File name", nullable = false, length = 250)
	private String fileName;
	@Column(name = "File_Heading", length = 250)
	private String fileHeading;
	@Column(name = "Description", length = 500)
	private String description;
	@Column(name = "File_Path", length = 250)
	private String filePath;
	@Column(name = "Status", nullable = false)
	private boolean status;

}
