package com.servana.domain;


import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * ServanaUserRole
 */
@Entity
@Table(name = "servana_user_role")
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel(description = "All details about the User Role. ")
@JsonIgnoreProperties(value = { "createdBy", "createdAt", "updatedBy", "updatedAt", "deletedBy", "deletedAt" })
public class ServanaUserRole extends Auditable<Long> implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6799593238982758585L;
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "Role_Id", unique = true, nullable = false)
	private Long roleId;
	
	@Column(name = "Role_Name", length = 50)
	@ApiModelProperty(notes = "Name should have atleast 50 characters")
	private String roleName;
	
	@Column(name = "Description", length = 150)
	@ApiModelProperty(notes = "Description should have atleast 150 characters")
	private String description;
	
	@Column(name = "Status")
	@ApiModelProperty(notes = "Role Status")
	private Boolean status;
	
}
