package com.servana.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@ToString
public class UserDetailsDTO {

	private long userId;
	private String emailId;
	private boolean status;
	private long hospitalId;
	private String firstName;
	private String lastName;
	private byte[] profilePicture;
	private String primaryPhone;
	private String secondaryPhone;
	private String address1;
	private String address2;
	private String city;
	private String state;
	private String country;
	private String zipCode;
	
	private String password;
	private String token;
	private String resetPasswordToken;
	
}
