package com.servana.exception;

import java.io.IOException;
import java.nio.file.AccessDeniedException;
import java.time.LocalDateTime;
import java.util.Arrays;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(value = { ResourceNotFoundException.class })
	protected ResponseEntity<Object> handleResourceNotFound(ResourceNotFoundException ex, WebRequest request) {

		ErrorDetails errorDetails = new ErrorDetails();
		errorDetails.setStatus(HttpStatus.NOT_FOUND);
		errorDetails.setTimestamp(LocalDateTime.now());
		errorDetails.setMessage(ex.getLocalizedMessage());
		errorDetails.setDetails(request.getDescription(false));
		errorDetails.setDebugMessage(Arrays.toString(ex.getStackTrace()));
		return new ResponseEntity<>(errorDetails, HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler(value = { AccessDeniedException.class })
	public ResponseEntity<Object> handleAccessDeniedException(AccessDeniedException ex, WebRequest request) {

		ErrorDetails errorDetails = new ErrorDetails();
		errorDetails.setStatus(HttpStatus.FORBIDDEN);
		errorDetails.setTimestamp(LocalDateTime.now());
		errorDetails.setMessage(ex.getLocalizedMessage());
		errorDetails.setDetails(request.getDescription(false));
		errorDetails.setDebugMessage(Arrays.toString(ex.getStackTrace()));
		return new ResponseEntity<>(errorDetails, HttpStatus.FORBIDDEN);
	}

	@ExceptionHandler(IOException.class)
	public ResponseEntity<Object> handleIOException(IOException ex, WebRequest request) {
		ErrorDetails errorDetails = new ErrorDetails();
		errorDetails.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
		errorDetails.setTimestamp(LocalDateTime.now());
		errorDetails.setMessage(ex.getLocalizedMessage());
		errorDetails.setDetails(request.getDescription(false));
		errorDetails.setDebugMessage(Arrays.toString(ex.getStackTrace()));
		return new ResponseEntity<>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(Exception.class)
	public ResponseEntity<Object> handle(Exception ex, WebRequest request) {

		ErrorDetails errorDetails = new ErrorDetails();
		errorDetails.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
		errorDetails.setTimestamp(LocalDateTime.now());
		errorDetails.setMessage(ex.getLocalizedMessage());
		errorDetails.setDetails(request.getDescription(false));
		errorDetails.setDebugMessage(Arrays.toString(ex.getStackTrace()));

		if (ex instanceof NullPointerException) {
			return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
	}
}
