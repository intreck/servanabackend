package com.servana.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.servana.domain.ServanaHpLoginUsers;

public interface ServanaHpLoginUsersRepository extends JpaRepository<ServanaHpLoginUsers, Long> {

	ServanaHpLoginUsers findByEmailId(String emailId);

}
