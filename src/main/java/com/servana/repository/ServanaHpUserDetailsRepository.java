package com.servana.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.servana.domain.ServanaHpUserDetails;

public interface ServanaHpUserDetailsRepository extends JpaRepository<ServanaHpUserDetails, Long> {

	ServanaHpUserDetails findByEmailId(String emailId);
	
}
