package com.servana.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.servana.domain.ServanaUserRole;

public interface ServanaUserRoleRepository extends JpaRepository<ServanaUserRole, Long> {
	
	ServanaUserRole findByRoleName(String name);
}
