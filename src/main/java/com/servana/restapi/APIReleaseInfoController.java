package com.servana.restapi;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.servana.aspect.LogExecutionTime;

@RestController
@RequestMapping("/api-info/")
public class APIReleaseInfoController {

	@Value( "${api.version}" )
	private String version;
	
	@Value( "${api.release-info}" )
	private String releaseInfo;
	
	
	/**
	 * confirmation
	 * 
	 * @param emailId
	 * @return
	 */
	@GetMapping(value = "/version", produces = "application/json")
	@LogExecutionTime
	public ResponseEntity<?> version() {
		return new ResponseEntity<>(version, HttpStatus.OK);
	}

	/**
	 * confirmation
	 * 
	 * @param emailId
	 * @return
	 */
	@GetMapping(value = "/release-info", produces = "application/json")
	@LogExecutionTime
	public ResponseEntity<?> releaseInfo() {
		return new ResponseEntity<>(releaseInfo, HttpStatus.OK);
	}
}
