package com.servana.restapi;

import static com.servana.constant.ApplicationConstant.ACCOUNT_EXPIRATION_TIME;
import static com.servana.constant.ApplicationConstant.BCRYPT_ID_FOR_ENCODE;

import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.servana.aspect.LogExecutionTime;
import com.servana.domain.Email;
import com.servana.domain.ServanaHpLoginUsers;
import com.servana.domain.ServanaHpUserDetails;
import com.servana.dto.UserDetailsDTO;
import com.servana.exception.ErrorDetails;
import com.servana.exception.ResourceNotFoundException;
import com.servana.service.IEmailService;
import com.servana.service.IServanaUserRoleService;
import com.servana.service.IServanaHpLoginUsersService;
import com.servana.service.IServanaHpUserDetailsService;
import com.servana.service.impl.MailContentBuilder;
import com.servana.util.RandomStringUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;


@Api(value = "Users", produces = "application/json", consumes = "application/json")
@RestController
@RequestMapping("/api1/users")
@Slf4j
public class ServanaUserController {	
	
	
	private @Autowired IServanaUserRoleService servanaUserRoleService;
	private @Autowired IServanaHpLoginUsersService servanaHpLoginUsersService;
	private @Autowired IServanaHpUserDetailsService servanaHpUserDetailsService;
	private @Autowired IEmailService emailService;
	private @Autowired MailContentBuilder mailContentBuilder;
	
	private @Value("${spring.mail.username}") String emailFromAddress;
	private @Value("${frontend.url}") String frontEndUrl;
	
	private @Value("${restapi.user.duplicate.message}") String restapi_user_duplicate_message;
	private @Value("${restapi.user.mail.register.message}") String restapi_user_mail_register_message;
	private @Value("${restapi.user.mail.register.heading}") String restapi_user_mail_register_heading;
	private @Value("${restapi.user.mail.retrieve.pwd.message}") String restapi_user_mail_retrieve_pwd_message;
	private @Value("${restapi.user.mail.retrieve.pwd.heading}") String restapi_user_mail_retrieve_pwd_heading;
	private @Value("${restapi.user.mail.retrieve.pwd.body}") String restapi_user_mail_retrieve_pwd_body;
	private @Value("${restapi.user.mail.reset.pwd.expired}") String restapi_user_mail_reset_pwd_expired;
	private @Value("${restapi.user.mail.changed.pwd.success}") String restapi_user_mail_changed_pwd_success;
	private @Value("${restapi.user.mail.confirmation.msg.expired}") String restapi_user_mail_confirmation_msg_expired;
	private @Value("${restapi.user.mail.account.verified}") String restapi_user_mail_account_verified;
	private @Value("${restapi.user.mail.account.already.verified}") String restapi_user_mail_account_already_verified;
	
	/**
	 * 
	 * @return
	 */
	@ApiOperation(value = "View a list of available users", responseContainer = "List", response = ServanaHpUserDetails.class, produces = "application/json")
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Successfully retrieved list"), 
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@LogExecutionTime
	@GetMapping(value = { "", "/" }, produces = "application/json")
	public List<ServanaHpUserDetails> list() {
		return servanaHpUserDetailsService.findAll();
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	@ApiOperation(value = "Search a User with an id", response = ServanaHpUserDetails.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successfully retrieved Servana User"), 
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@GetMapping(value = "/{id}", produces = "application/json")
	@LogExecutionTime
	public ServanaHpUserDetails findById(@ApiParam(value = "User Id.", required = true) @PathVariable(value = "id") long id) {
		
		return servanaHpUserDetailsService.findById(id).orElseThrow(() -> new ResourceNotFoundException("User", "id", id));
	}

	/**
	 * 
	 * @param user
	 * @return
	 */
	@ApiOperation(value = "Add a User")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successfully saved User"), 
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@PostMapping(produces = "application/json")
	@LogExecutionTime	
	public ResponseEntity<?> save(@ApiParam(value = "User Details.", required = true) @Valid @RequestBody UserDetailsDTO userDetailsDTO) {
		log.info(":: user = {} ::", userDetailsDTO);
		ServanaHpLoginUsers userExists = servanaHpLoginUsersService.findByEmailId(userDetailsDTO.getEmailId());
		
		if ( null != userExists ) {
			ErrorDetails errorDetails = new ErrorDetails();
			errorDetails.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
			errorDetails.setTimestamp(LocalDateTime.now());
			errorDetails.setMessage(restapi_user_duplicate_message);

			return new ResponseEntity<>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		ServanaHpLoginUsers servanaHpLoginUsers = new ServanaHpLoginUsers();
		servanaHpLoginUsers.setEmailId(userDetailsDTO.getEmailId());
		servanaHpLoginUsers.setPassword(BCRYPT_ID_FOR_ENCODE + new BCryptPasswordEncoder().encode(userDetailsDTO.getPassword()));
		servanaHpLoginUsers.setServanaUserRole(servanaUserRoleService.findByName("ROLE_USER"));
		servanaHpLoginUsers.setIsAccountNonExpired(true);
		servanaHpLoginUsers.setIsAccountNonLocked(true);
		servanaHpLoginUsers.setIsActive(true);
		servanaHpLoginUsers.setIsCredentialsNonExpired(true);
		servanaHpLoginUsers.setIsVerified(false);
		
		String token = RandomStringUtil.getAlphaNumericString(35);
		servanaHpLoginUsers.setToken(token);
		
		ServanaHpLoginUsers savedUser = servanaHpLoginUsersService.save(servanaHpLoginUsers);
		Email emailObj = new Email();
		emailObj.setFrom( emailFromAddress );
		emailObj.setTo( savedUser.getEmailId() );
		emailObj.setSubject(" Verify email address " );		
		
		Map<String, String> map = new HashMap<>();
		map.put("name", savedUser.getEmailId());
		map.put("heading", restapi_user_mail_register_heading);
		map.put("message", restapi_user_mail_register_message);		
		map.put("buttonName", restapi_user_mail_register_heading);
		map.put("verifyUrl", frontEndUrl + "#/signup/confirmfinish?emailId=" + savedUser.getEmailId() + "&token=" + token );
		
		String content = mailContentBuilder.retrieveNewUserConfirmation(map);
		emailObj.setText(content);
		log.info(":: content = {} ::", content);
		
		emailService.sendSimpleMessage(emailObj);
		
		log.info(":: Saved User = {} ::", savedUser);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(savedUser.getLoginUsersId()).toUri();

		return ResponseEntity.created(location).build();

	}

	/**
	 * 
	 * @param id
	 * @param user
	 * @return
	 */
	@ApiOperation(value = "Update a User")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successfully Updated User"), 
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@PutMapping(value = "/{id}", produces = "application/json")
	@LogExecutionTime
	public ResponseEntity<?> update(@ApiParam(value = "User Id.", required = true) @PathVariable(value = "id") Long id, @Valid @RequestBody UserDetailsDTO userDetailsDTO) {		
		Optional<ServanaHpUserDetails> userOptional = servanaHpUserDetailsService.findById(id);

		if (!userOptional.isPresent())
			return ResponseEntity.notFound().build();
		
		ServanaHpUserDetails servanaHpUserDetails = new ServanaHpUserDetails();
		servanaHpUserDetails.setUserId(id);
		servanaHpUserDetails.setEmailId(userDetailsDTO.getEmailId());
		try {
			
			BeanUtils.copyProperties(servanaHpUserDetails, userDetailsDTO);
		} catch (IllegalAccessException | InvocationTargetException e) {
			e.printStackTrace();
		}
		
		servanaHpUserDetails.setUserId(id);

		servanaHpUserDetailsService.save(servanaHpUserDetails);

		return ResponseEntity.noContent().build();

	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	@ApiOperation(value = "Delete a User")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successfully Deleted User"), 
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@DeleteMapping(value = "/{id}", produces = "application/json")
	@LogExecutionTime
	public ResponseEntity<?> delete(@ApiParam(value = "User Id.", required = true)@PathVariable(value = "id") Long id) {		
		ServanaHpUserDetails role = servanaHpUserDetailsService.findById(id).orElseThrow(() -> new ResourceNotFoundException("User", "id", id));

		servanaHpUserDetailsService.delete(role);

		return ResponseEntity.ok().build();
	}
	
	/**
	 * findByEmailId
	 * @param emailId
	 * @return
	 */
	@ApiOperation(value = "Search a User with an EmailId", response = ServanaHpUserDetails.class)
	@GetMapping(value = "/findByEmailId", produces = "application/json")
	@LogExecutionTime
	public ServanaHpLoginUsers findByEmailId(@ApiParam(value = "User EmailId.", required = true) @RequestParam(value = "emailId") String emailId) {
		try {
			Optional<ServanaHpLoginUsers> user = Optional.of(servanaHpLoginUsersService.findByEmailId(emailId));
			if (user.isPresent()) {
				return user.get();
			} else {
				throw new ResourceNotFoundException("User", "emailId", emailId);
			}
		} catch (Exception e) {
			throw new ResourceNotFoundException("User", "emailId", emailId);
		}
	}
	
	/**
	 * forgotPassword
	 * @param user
	 * @return
	 */
	@ApiOperation(value = "Forgot Your Password")
	@PostMapping(value = "/forgotPassword", produces = "application/json")
	@LogExecutionTime
	public ResponseEntity<?> forgotPassword(@ApiParam(value = "User Details.", required = true) @Valid @RequestBody UserDetailsDTO userDetailsDTO) {
		log.info(":: User = {} ::", userDetailsDTO);
		ErrorDetails errorDetails = new ErrorDetails();
		String emailId = userDetailsDTO.getEmailId();
		ServanaHpLoginUsers findServanaHpLoginUsers = servanaHpLoginUsersService.findByEmailId(emailId);
		if (null != findServanaHpLoginUsers) {
			try {
				
				findServanaHpLoginUsers.setUpdatedAt(new Date());
				
				String token = RandomStringUtil.getAlphaNumericString(35);
				findServanaHpLoginUsers.setResetPasswordToken(token);
				
				ServanaHpLoginUsers savedUser = servanaHpLoginUsersService.save(findServanaHpLoginUsers);
				ServanaHpUserDetails servanaHpUserDetails = servanaHpUserDetailsService.findByEmailId(emailId);
				String fullName = "";
				if ( servanaHpUserDetails == null ) {
					servanaHpUserDetails = new ServanaHpUserDetails();
					String fName = servanaHpUserDetails.getFirstName();
					String lName = servanaHpUserDetails.getLastName();
					fullName = (fName == null ? "" : fName) + " " + (lName == null ? "" : lName);
				}
				
				Email emailObj = new Email();
				emailObj.setFrom( emailFromAddress );
				emailObj.setTo( savedUser.getEmailId() );
				emailObj.setSubject(" Reset Password " );
				
				Map<String, String> map = new HashMap<>();
				map.put("name", fullName);
				map.put("heading", restapi_user_mail_retrieve_pwd_heading);
				map.put("message", restapi_user_mail_retrieve_pwd_message);			
				map.put("buttonName", restapi_user_mail_retrieve_pwd_heading);
				map.put("verifyUrl", frontEndUrl + "#/signup/resetpassword?emailId=" + savedUser.getEmailId() + "&token=" + token );
				
				String content = mailContentBuilder.retrieveResetPassword(map);
				emailObj.setText(content);
				log.info(":: content = {} ::", content);
				
				emailService.sendSimpleMessage(emailObj);
				
				errorDetails.setStatus(HttpStatus.OK);
				errorDetails.setTimestamp(LocalDateTime.now());
				errorDetails.setMessage(restapi_user_mail_retrieve_pwd_body);
			} catch (Exception e) {
				log.error(":: Exception occured in forgotPassword() ::", e);
			}
			
			return new ResponseEntity<>(errorDetails, HttpStatus.OK);
		} else {
			throw new ResourceNotFoundException("User", "EmailId ", emailId);
		}
	}
	
	/**
	 * confirmation
	 * 
	 * @param emailId
	 * @return
	 */
	@PostMapping(value = "/resetPassword", produces = "application/json")
	@ApiOperation(value = "Reset Your Password")
	@LogExecutionTime
	public ResponseEntity<?> resetPassword(@ApiParam(value = "User Details.", required = true) @Valid @RequestBody UserDetailsDTO userDetailsDTO) {
		log.info(":: user = {} ::", userDetailsDTO);
		ErrorDetails errorDetails = new ErrorDetails();
		try {
			ServanaHpLoginUsers findUser = servanaHpLoginUsersService.findByEmailId(userDetailsDTO.getEmailId());
			if (null == findUser) {
				throw new ResourceNotFoundException("User", "emailId", userDetailsDTO.getEmailId());
			}
			
			if ( !findUser.getResetPasswordToken().equals(userDetailsDTO.getResetPasswordToken()) ) {
				throw new ResourceNotFoundException("User", "resetPasswordToken", userDetailsDTO.getResetPasswordToken());
			}

			Date createdDate = findUser.getUpdatedAt();
			long diffInMillies = Math.abs(new Date().getTime() - createdDate.getTime());

			log.info(":: diff = {} ::", diffInMillies);
			if (diffInMillies > ACCOUNT_EXPIRATION_TIME) {
				log.info(restapi_user_mail_reset_pwd_expired);

				errorDetails.setStatus(HttpStatus.OK);
				errorDetails.setTimestamp(LocalDateTime.now());
				errorDetails.setMessage(restapi_user_mail_reset_pwd_expired);

				return new ResponseEntity<>(errorDetails, HttpStatus.OK);
			} else {
				String password = userDetailsDTO.getPassword();
				findUser.setPassword(BCRYPT_ID_FOR_ENCODE + new BCryptPasswordEncoder().encode(password));
				findUser.setUpdatedAt(new Date());
				servanaHpLoginUsersService.save(findUser);
				
				errorDetails.setStatus(HttpStatus.OK);
				errorDetails.setTimestamp(LocalDateTime.now());
				errorDetails.setMessage(restapi_user_mail_changed_pwd_success);

				return new ResponseEntity<>(errorDetails, HttpStatus.OK);
			}
		} catch (Exception e) {
			log.error(":: Internal Server Error Occured.  ::", e);
			errorDetails.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
			errorDetails.setTimestamp(LocalDateTime.now());
			errorDetails.setMessage("Internal Server Error Occured.");

			return new ResponseEntity<>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * confirmation
	 * 
	 * @param emailId
	 * @return
	 */
	@GetMapping(value = "/confirmation", produces = "application/json")
	@ApiOperation(value = "User Registration Confirmation")
	@LogExecutionTime
	public ResponseEntity<?> confirmation(@RequestParam(value = "emailId") String emailId, @RequestParam(value = "token") String token) {
		Date currentDate = new Date();
		ErrorDetails errorDetails = new ErrorDetails();
		try {
			ServanaHpLoginUsers servanaHpLoginUsers = servanaHpLoginUsersService.findByEmailId(emailId);
			if (null == servanaHpLoginUsers) {
				throw new ResourceNotFoundException("ServanaHpLoginUsers", "emailId", emailId);
			}

			Date createdDate = servanaHpLoginUsers.getCreatedAt();
			long diffInMillies = Math.abs(currentDate.getTime() - createdDate.getTime());

			log.info(":: diff = {} ::", diffInMillies);
			if (diffInMillies > ACCOUNT_EXPIRATION_TIME) {
				log.info(restapi_user_mail_confirmation_msg_expired);

				errorDetails.setStatus(HttpStatus.OK);
				errorDetails.setTimestamp(LocalDateTime.now());
				errorDetails.setMessage(restapi_user_mail_confirmation_msg_expired);

				return new ResponseEntity<>(errorDetails, HttpStatus.OK);
			} else {
				if ( !servanaHpLoginUsers.getIsVerified() ) {
					servanaHpLoginUsers.setIsVerified(true);
					servanaHpLoginUsersService.save(servanaHpLoginUsers);
					
					errorDetails.setStatus(HttpStatus.OK);
					errorDetails.setTimestamp(LocalDateTime.now());
					errorDetails.setMessage(restapi_user_mail_account_verified);
				} else {
					errorDetails.setStatus(HttpStatus.OK);
					errorDetails.setTimestamp(LocalDateTime.now());
					errorDetails.setMessage(restapi_user_mail_account_already_verified);
				}				

				return new ResponseEntity<>(errorDetails, HttpStatus.OK);
			}
		} catch (Exception e) {
			errorDetails.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
			errorDetails.setTimestamp(LocalDateTime.now());
			errorDetails.setMessage("Internal Server Error Occured.");

			return new ResponseEntity<>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
}
