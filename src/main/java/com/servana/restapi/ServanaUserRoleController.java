package com.servana.restapi;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.servana.aspect.LogExecutionTime;
import com.servana.domain.ServanaUserRole;
import com.servana.exception.ResourceNotFoundException;
import com.servana.service.IServanaUserRoleService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/api/roles")
@Api(value = "Roles", produces = "application/json", consumes = "application/json")
public class ServanaUserRoleController {
	
	private static final Logger logger = LoggerFactory.getLogger(ServanaUserRoleController.class);
	
	private @Autowired IServanaUserRoleService servanaUserRoleService;
	
	@GetMapping( value = "/{id}")	
	@ApiOperation(value = "View a list of available Servana User Role by Id", response = ServanaUserRole.class, produces = "application/json")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successfully retrieved Servana User Role"), 
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@LogExecutionTime
	public ServanaUserRole findById(@PathVariable(value = "id") long id) {
		logger.info(":: Entered into findById() method ::");
		return servanaUserRoleService.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("ServanaUserRole", "id", id));
	}
	
	
	@GetMapping(value = {"", "/"})
	@ApiOperation(value = "View a list of available Servana User Role by Id", responseContainer = "List",  response = ServanaUserRole.class, produces = "application/json")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successfully retrieved list"), 
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@LogExecutionTime
	public List<ServanaUserRole> findAll() {
		logger.info(":: Entered into findAll() method ::");
	    return servanaUserRoleService.findAll();
	}
	
	/**
	 * 
	 * @param role
	 * @return
	 */
	@PostMapping
	@ApiOperation(value = "Add a User Role")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successfully saved User Role"), 
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@LogExecutionTime
	public ServanaUserRole save(@Valid @RequestBody ServanaUserRole role) {
		logger.info(":: Entered into save() method ::");
	    return servanaUserRoleService.save(role);
	}
	
	/**
	 * 
	 * @param id
	 * @param role
	 * @return
	 */
	@PutMapping("/{id}")
	@ApiOperation(value = "Update a User Role")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successfully Updated User Role"), 
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@LogExecutionTime
	public ServanaUserRole update(@PathVariable(value = "id") Long id, @Valid @RequestBody ServanaUserRole role) {
		logger.info(":: Entered into update() method ::");
		ServanaUserRole sRole = servanaUserRoleService.findById(id)
							.orElseThrow(() -> new ResourceNotFoundException("ServanaUserRole", "id", id));
		
		sRole.setRoleName(role.getRoleName());
		sRole.setDescription(role.getDescription());
		
		return servanaUserRoleService.save(sRole);
	}
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	@DeleteMapping("/{id}")
	@ApiOperation(value = "Delete a User Role")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successfully Deleted User Role"), 
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@LogExecutionTime
	public ResponseEntity<?> delete(@PathVariable(value = "id") Long id) {
		logger.info(":: Entered into delete() method ::");
		ServanaUserRole role = servanaUserRoleService.findById(id)
	            		.orElseThrow(() -> new ResourceNotFoundException("ServanaUserRole", "id", id));

		servanaUserRoleService.delete(role);

	    return ResponseEntity.ok().build();
	}
	
}
