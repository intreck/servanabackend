package com.servana.security;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.access.AccessDecisionVoter;
import org.springframework.security.access.hierarchicalroles.RoleHierarchy;
import org.springframework.security.access.hierarchicalroles.RoleHierarchyImpl;
import org.springframework.security.access.vote.AffirmativeBased;
import org.springframework.security.access.vote.RoleHierarchyVoter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.provider.error.OAuth2AccessDeniedHandler;
import org.springframework.security.oauth2.provider.expression.OAuth2WebSecurityExpressionHandler;
import org.springframework.security.web.access.expression.WebExpressionVoter;

@Configuration
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {
	
	@Bean
    public RoleHierarchyImpl roleHierarchy() {
        RoleHierarchyImpl roleHierarchy = new RoleHierarchyImpl();
        roleHierarchy.setHierarchy("ROLE_ADMIN > ROLE_USER");    
        
        return roleHierarchy;
    }
	
	@Bean
    public RoleHierarchyVoter roleVoter() {
        return new RoleHierarchyVoter(roleHierarchy());
    }
	
	@Bean
	public AffirmativeBased defaultOauthDecisionManager(RoleHierarchy roleHierarchy) {

		List<AccessDecisionVoter<? extends Object>> decisionVoters = new ArrayList<>();

		// WebExpressionVoter
		OAuth2WebSecurityExpressionHandler expressionHandler = new OAuth2WebSecurityExpressionHandler();
		expressionHandler.setRoleHierarchy(roleHierarchy);

		WebExpressionVoter webExpressionVoter = new WebExpressionVoter();
		webExpressionVoter.setExpressionHandler(expressionHandler);

		decisionVoters.add(webExpressionVoter);
		decisionVoters.add(roleVoter());

		return new AffirmativeBased(decisionVoters);
	}

	@Override
	public void configure(HttpSecurity http) throws Exception {
		
		//-- define URL patterns to enable OAuth2 security
		http.anonymous().disable()
			.requestMatchers().antMatchers("/api/**")
			.and().authorizeRequests()
			.accessDecisionManager(defaultOauthDecisionManager(roleHierarchy())) //Role Hierarch with OAuth2
			//.antMatchers("/api/**").access("hasRole('ADMIN') or hasRole('USER')")
			.antMatchers("/api/**").authenticated()
			//.anyRequest().authenticated()
			.and().exceptionHandling().accessDeniedHandler(new OAuth2AccessDeniedHandler());
	}
	
}
