package com.servana.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class ServanaAuthenticationProvider implements AuthenticationProvider {
	
	@Autowired
    private UserDetailsService userDetailsService;
	
	@Autowired
	private PasswordEncoder passwordEncoder;

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		
		String username = authentication.getPrincipal().toString();
		String password = authentication.getCredentials().toString();
		
		UserDetails userDetail = userDetailsService.loadUserByUsername(username);
		if (userDetail == null) {
			throw new UsernameNotFoundException(String.format("The credentials you supplied are invalid.", authentication.getPrincipal()));
		}
		
		if(!userDetail.isEnabled()) {
			
			throw new DisabledException((String.format("Your account is disabled, please contact Administrator.")));
		}
		
		if (!passwordEncoder.matches(password, userDetail.getPassword())) {
			
			throw new BadCredentialsException(String.format("Password entered is invalid."));
		}
		
		UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(userDetail, null, userDetail.getAuthorities());
		
		return token;
	}

	@Override
	public boolean supports(Class<?> aClass) {
		
		return UsernamePasswordAuthenticationToken.class.isAssignableFrom(aClass);
	}

}
