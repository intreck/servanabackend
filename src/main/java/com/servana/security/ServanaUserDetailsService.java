package com.servana.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User.UserBuilder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.servana.domain.ServanaHpLoginUsers;
import com.servana.service.IServanaHpLoginUsersService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ServanaUserDetailsService implements UserDetailsService {

	@Autowired
	private IServanaHpLoginUsersService servanaHpLoginUsersService;

	@Override
	public UserDetails loadUserByUsername(String emailId) throws UsernameNotFoundException {

		ServanaHpLoginUsers user = servanaHpLoginUsersService.findByEmailId(emailId);
		log.info("========================" + user);
		UserBuilder builder = null;
		if (user == null) {

			throw new UsernameNotFoundException("EmailId " + emailId + " not found");
		} else {

			builder = org.springframework.security.core.userdetails.User.withUsername(emailId);
			builder.password(user.getPassword());
			builder.disabled(!user.getIsActive());
			builder.credentialsExpired(!user.getIsCredentialsNonExpired());
			builder.accountLocked(!user.getIsAccountNonLocked());
			builder.accountExpired(!user.getIsAccountNonExpired());
            //builder.roles(user.getRoles().stream().map(role -> role.getName().substring(role.getName().indexOf("_") + 1)).collect(Collectors.joining(",")));
            //builder.authorities(user.getRoles().stream().map(role -> role.getName()).collect(Collectors.joining(",")));
			builder.authorities(user.getServanaUserRole().getRoleName());
		}

		return builder.build();
	}

}
