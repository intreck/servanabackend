package com.servana.service;

import com.servana.domain.Email;

public interface IEmailService {

	void sendSimpleMessage(Email email);
	
}
