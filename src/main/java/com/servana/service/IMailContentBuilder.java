package com.servana.service;

import java.util.Map;

public interface IMailContentBuilder {

	String retrieveNewUserConfirmation(Map<String, String> map);
	
	String retrieveResetPassword(Map<String, String> map);
	
}
