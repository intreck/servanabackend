package com.servana.service;

import java.util.List;
import java.util.Optional;

import com.servana.domain.ServanaHpLoginUsers;

public interface IServanaHpLoginUsersService {

	ServanaHpLoginUsers save(ServanaHpLoginUsers servanaHpLoginUsers);
	
	Optional<ServanaHpLoginUsers> findById(long id);
	
	boolean existsById(long id);

	List<ServanaHpLoginUsers> findAll();

	long count();
	
	void deleteById(long id);
	
	void delete(ServanaHpLoginUsers servanaHpLoginUsers);

	void deleteAll();
	
	ServanaHpLoginUsers findByEmailId(String emailId);
}
