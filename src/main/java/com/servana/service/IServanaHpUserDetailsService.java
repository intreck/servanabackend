package com.servana.service;

import java.util.List;
import java.util.Optional;

import com.servana.domain.ServanaHpUserDetails;

public interface IServanaHpUserDetailsService {

	ServanaHpUserDetails save(ServanaHpUserDetails servanaHpUserDetails);
	
	Optional<ServanaHpUserDetails> findById(long id);
	
	boolean existsById(long id);

	List<ServanaHpUserDetails> findAll();

	long count();
	
	void deleteById(long id);
	
	void delete(ServanaHpUserDetails servanaHpUserDetails);

	void deleteAll();
	
	ServanaHpUserDetails findByEmailId(String emailId);
}
