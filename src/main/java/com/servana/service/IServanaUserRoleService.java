package com.servana.service;

import java.util.List;
import java.util.Optional;

import com.servana.domain.ServanaUserRole;

public interface IServanaUserRoleService {

	ServanaUserRole save(ServanaUserRole entity);

	Optional<ServanaUserRole> findById(long id);

	boolean existsById(long id);

	List<ServanaUserRole> findAll();

	long count();

	void deleteById(long id);

	void delete(ServanaUserRole entity);

	void deleteAll();

	ServanaUserRole findByName(String name);
}
