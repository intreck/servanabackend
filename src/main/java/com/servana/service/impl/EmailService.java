package com.servana.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;

import com.servana.domain.Email;
import com.servana.service.IEmailService;

@Service
public class EmailService implements IEmailService {

	private JavaMailSender javaMailSender;
	
	@Autowired
	public void setJavaMailSender(JavaMailSender javaMailSender) {
		this.javaMailSender = javaMailSender;
	}
	
	@Override
	public void sendSimpleMessage(Email email) {
		
		MimeMessagePreparator messagePreparator = mimeMessage -> {
			
			mimeMessage.setContent(email.getText(), "text/html");
			
			MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage, false, "UTF-8");
			messageHelper.setFrom(email.getFrom());
			messageHelper.setTo(email.getTo());
			messageHelper.setSubject(email.getSubject());
			
		};

		javaMailSender.send(messagePreparator);
	}

}
