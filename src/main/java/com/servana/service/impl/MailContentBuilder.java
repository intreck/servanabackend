package com.servana.service.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import com.servana.service.IMailContentBuilder;

@Service
public class MailContentBuilder implements IMailContentBuilder {

	@Autowired
	private TemplateEngine templateEngine;

	@Override
	public String retrieveNewUserConfirmation(Map<String, String> map) {
		Context context = new Context();

		for (String key : map.keySet()) {
			context.setVariable(key, map.get(key));
		}

		return templateEngine.process("email/NewUserConfirmation", context);
	}

	@Override
	public String retrieveResetPassword(Map<String, String> map) {

		Context context = new Context();

		for (String key : map.keySet()) {
			context.setVariable(key, map.get(key));
		}

		return templateEngine.process("email/ResetPassword", context);
	}
}
