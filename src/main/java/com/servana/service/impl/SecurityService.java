package com.servana.service.impl;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import com.servana.domain.ServanaHpLoginUsers;
import com.servana.service.ISecurityService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class SecurityService implements ISecurityService {

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private UserDetailsService userDetailsService;

	@Override
	public void autologin(String emailId, String password) {
		ServanaHpLoginUsers profile = (ServanaHpLoginUsers) userDetailsService.loadUserByUsername(emailId);
		UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(profile, password,
				Arrays.asList(new SimpleGrantedAuthority(profile.getServanaUserRole().getRoleName())));

		authenticationManager.authenticate(token);

		if (token.isAuthenticated()) {
			SecurityContextHolder.getContext().setAuthentication(token);
			log.info(String.format("Auto login %s successfully!", emailId));
		}
	}
}
