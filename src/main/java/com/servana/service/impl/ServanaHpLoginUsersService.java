package com.servana.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.servana.domain.ServanaHpLoginUsers;
import com.servana.repository.ServanaHpLoginUsersRepository;
import com.servana.service.IServanaHpLoginUsersService;

@Service
@Transactional
public class ServanaHpLoginUsersService implements IServanaHpLoginUsersService {

	private ServanaHpLoginUsersRepository servanaHpLoginUsersRepository;

	@Autowired
	public void setServanaHpLoginUsersRepository(ServanaHpLoginUsersRepository servanaHpLoginUsersRepository) {
		this.servanaHpLoginUsersRepository = servanaHpLoginUsersRepository;
	}

	@Transactional(propagation = Propagation.REQUIRED)
	@Override
	public ServanaHpLoginUsers save(ServanaHpLoginUsers user) {
		return servanaHpLoginUsersRepository.save(user);
	}

	@Override
	public Optional<ServanaHpLoginUsers> findById(long id) {
		return servanaHpLoginUsersRepository.findById(id);
	}

	@Override
	public boolean existsById(long id) {
		return servanaHpLoginUsersRepository.existsById(id);
	}

	@Override
	public List<ServanaHpLoginUsers> findAll() {

		return servanaHpLoginUsersRepository.findAll();
	}

	@Override
	public long count() {
		return servanaHpLoginUsersRepository.count();
	}

	@Override
	public void deleteById(long id) {
		servanaHpLoginUsersRepository.deleteById(id);
	}

	@Override
	public void delete(ServanaHpLoginUsers user) {
		servanaHpLoginUsersRepository.delete(user);
	}

	@Override
	public void deleteAll() {
		servanaHpLoginUsersRepository.deleteAll();
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	@Override
	public ServanaHpLoginUsers findByEmailId(String emailId) {

		return servanaHpLoginUsersRepository.findByEmailId(emailId);
	}
}
