package com.servana.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.servana.domain.ServanaHpUserDetails;
import com.servana.repository.ServanaHpUserDetailsRepository;
import com.servana.service.IServanaHpUserDetailsService;

@Service
@Transactional
public class ServanaHpUserDetailsService implements IServanaHpUserDetailsService {
	
	private ServanaHpUserDetailsRepository servanaHpUserDetailsRepository;
	
	@Autowired	
	public void setServanaHpUserDetailsRepository(ServanaHpUserDetailsRepository servanaHpUserDetailsRepository) {
		this.servanaHpUserDetailsRepository = servanaHpUserDetailsRepository;
	}

	@Override
	public ServanaHpUserDetails save(ServanaHpUserDetails servanaHpUserDetails) {
		
		return servanaHpUserDetailsRepository.save(servanaHpUserDetails);
	}

	@Override
	public Optional<ServanaHpUserDetails> findById(long id) {
		
		return servanaHpUserDetailsRepository.findById(id);
	}

	@Override
	public boolean existsById(long id) {
		
		return servanaHpUserDetailsRepository.existsById(id);
	}

	@Override
	public List<ServanaHpUserDetails> findAll() {
		
		return servanaHpUserDetailsRepository.findAll();
	}

	@Override
	public long count() {
		
		return servanaHpUserDetailsRepository.count();
	}

	@Override
	public void deleteById(long id) {
		
		servanaHpUserDetailsRepository.deleteById(id);
	}

	@Override
	public void delete(ServanaHpUserDetails servanaHpUserDetails) {
		
		servanaHpUserDetailsRepository.delete(servanaHpUserDetails);
	}

	@Override
	public void deleteAll() {
		
		servanaHpUserDetailsRepository.deleteAll();
	}

	@Override
	public ServanaHpUserDetails findByEmailId(String emailId) {
		
		return servanaHpUserDetailsRepository.findByEmailId(emailId);
	}

}
