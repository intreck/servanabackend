package com.servana.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.servana.domain.ServanaUserRole;
import com.servana.repository.ServanaUserRoleRepository;
import com.servana.service.IServanaUserRoleService;

@Service
@Transactional
public class ServanaUserRoleService implements IServanaUserRoleService {

	private ServanaUserRoleRepository servanaUserRoleRepository;

	
	@Autowired
	public void setServanaUserRoleRepository(ServanaUserRoleRepository servanaUserRoleRepository) {
		this.servanaUserRoleRepository = servanaUserRoleRepository;
	}

	@Override
	public ServanaUserRole save(ServanaUserRole servanaUserRole) {

		return servanaUserRoleRepository.save(servanaUserRole);
	}

	@Override
	public Optional<ServanaUserRole> findById(long id) {

		return servanaUserRoleRepository.findById(id);
	}

	@Override
	public boolean existsById(long id) {

		return servanaUserRoleRepository.existsById(id);
	}

	@Override
	public List<ServanaUserRole> findAll() {

		return (List<ServanaUserRole>) servanaUserRoleRepository.findAll();
	}

	@Override
	public long count() {

		return servanaUserRoleRepository.count();
	}

	@Override
	public void deleteById(long id) {

		servanaUserRoleRepository.deleteById(id);
	}

	@Override
	public void delete(ServanaUserRole servanaUserRole) {

		servanaUserRoleRepository.delete(servanaUserRole);
	}

	@Override
	public void deleteAll() {

		servanaUserRoleRepository.deleteAll();
	}

	@Override
	public ServanaUserRole findByName(String name) {

		return servanaUserRoleRepository.findByRoleName(name);
	}
}
