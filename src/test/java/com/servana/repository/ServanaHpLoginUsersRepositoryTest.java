package com.servana.repository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;

import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.servana.domain.ServanaHpLoginUsers;

@ActiveProfiles("dev")
@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = NONE)
public class ServanaHpLoginUsersRepositoryTest {

	@Autowired
	private ServanaHpLoginUsersRepository repository;

	@Test
	public void whenFindAll() {

		List<ServanaHpLoginUsers> list = repository.findAll();
		
		assertThat(list.size()).isGreaterThan(0);
	}

	@Test
	public void whenFindById() {
		// given
		ServanaHpLoginUsers servanaHpLoginUsers = new ServanaHpLoginUsers();
		servanaHpLoginUsers.setEmailId("test@test.com");
		servanaHpLoginUsers.setIsVerified(true);
		servanaHpLoginUsers = repository.save(servanaHpLoginUsers);

		// when
		Optional<ServanaHpLoginUsers> testObj = repository.findById(servanaHpLoginUsers.getLoginUsersId());

		// then
		assertThat(testObj.get().getEmailId()).isEqualTo(servanaHpLoginUsers.getEmailId());
	}

	@Test
	public void whenFindByEmailId() {
		// given
		ServanaHpLoginUsers servanaHpLoginUsers = new ServanaHpLoginUsers();
		servanaHpLoginUsers.setEmailId("test@test.com");
		servanaHpLoginUsers.setIsVerified(true);
		servanaHpLoginUsers = repository.save(servanaHpLoginUsers);

		// when
		ServanaHpLoginUsers testObj = repository.findByEmailId(servanaHpLoginUsers.getEmailId());

		// then
		assertThat(testObj.getEmailId()).isEqualTo(servanaHpLoginUsers.getEmailId());
	}

}
