package com.servana.repository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;

import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.servana.domain.ServanaUserRole;

import lombok.extern.slf4j.Slf4j;

@ActiveProfiles("dev")
@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = NONE)
@Slf4j
public class ServanaUserRoleRepositoryTest {

	@Autowired
	private ServanaUserRoleRepository repository;

	@Test
	public void whenFindAll() {

		List<ServanaUserRole> list = repository.findAll();

		assertThat(list.size()).isGreaterThan(0);
	}

	@Test
	public void whenFindById() {
		// given
		ServanaUserRole servanaUserRole = new ServanaUserRole();
		servanaUserRole.setRoleName("TEST_ROLE");		
		servanaUserRole = repository.save(servanaUserRole);

		// when
		Optional<ServanaUserRole> testObj = repository.findById(servanaUserRole.getRoleId());

		// then
		assertThat(testObj.get().getRoleId()).isEqualTo(servanaUserRole.getRoleId());
	}

	@Test
	public void whenFindByRoleName() {
		// given
		ServanaUserRole servanaUserRole = new ServanaUserRole();
		servanaUserRole.setRoleName("TEST_ROLE");		
		servanaUserRole = repository.save(servanaUserRole);

		// when
		ServanaUserRole testObj = repository.findByRoleName(servanaUserRole.getRoleName());

		// then
		assertThat(testObj.getRoleName()).isEqualTo(servanaUserRole.getRoleName());
	}
	
	@Test
	public void whenDelete() {
		// given
		ServanaUserRole servanaUserRole = new ServanaUserRole();
		servanaUserRole.setRoleName("TEST_ROLE");		
		servanaUserRole = repository.save(servanaUserRole);
		
		repository.delete(servanaUserRole);
		
		// when
		ServanaUserRole testObj = repository.findByRoleName(servanaUserRole.getRoleName());
		log.info(":: Test Object = {} ::", testObj);
		// then
		assertThat(testObj).isNull();
	}

}
