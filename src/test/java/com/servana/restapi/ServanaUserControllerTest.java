package com.servana.restapi;

import static java.util.Collections.singletonList;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.hamcrest.Matchers.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.servana.domain.ServanaHpUserDetails;

@RunWith(SpringRunner.class)
@WebMvcTest(value = ServanaUserController.class, secure = false)
public class ServanaUserControllerTest {

	@Autowired
	private MockMvc mvc;

	@MockBean
	private ServanaUserController controller;

	@Test
	public void whenList() throws Exception {
		ServanaHpUserDetails servanaHpUserDetails = new ServanaHpUserDetails();
		servanaHpUserDetails.setCity("Yerevan");

		List<ServanaHpUserDetails> list = singletonList(servanaHpUserDetails);

		given(controller.list()).willReturn(list);

		mvc.perform(get("/api1/users")
				.contentType(APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$", hasSize(1)))
				.andExpect(jsonPath("$[0].city", is(servanaHpUserDetails.getCity())))
				.andDo(print());
	}
}
